export interface User {
  _id: string,
  email: string,
  name: string,
  avatar: null | File,
  token: string
  role: string
}

export interface FieldError {
  message: string
}

export interface LoginUserData {
  email: string,
  password: string,
}

export interface LoginError {
  error: string,
}

export interface Publish {
  id: string,
  isPublished: boolean
}
