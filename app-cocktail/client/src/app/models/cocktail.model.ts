export class Cocktail {
  constructor(
    public id: string,
    public name: string,
    public user: {
      _id: string,
      name: string
    },
    public image: null | string,
    public recipe: string,
    public isPublished: boolean,
    public ingredients: [{
      ing: string,
      amount: string
    }]
  ) {}
}

export interface CocktailData {
  [key: string]: any,
  name: string,
  user: string,
  image: File | null,
  recipe: string,
  isPublished: boolean,
  ingredients: [{
    ing: string,
    amount: string
  }]
}

export interface ApiCocktailData {
  _id: string,
  name: string,
  user: {
    _id: string,
    name: string
  },
  image: null | string,
  recipe: string,
  isPublished: boolean,
  ingredients: [{
    ing: string,
    amount: string
  }]
}
