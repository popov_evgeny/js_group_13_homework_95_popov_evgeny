import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable, Subscription } from 'rxjs';
import { Cocktail } from '../../models/cocktail.model';
import { environment } from '../../../environments/environment';
import { User } from '../../models/user.model';
import { fetchUserCocktailRequest, publishRequest, removeCocktailRequest } from '../../store/cocktails.actions';

@Component({
  selector: 'app-user-cocktails',
  templateUrl: './user-cocktails.component.html',
  styleUrls: ['./user-cocktails.component.sass']
})
export class UserCocktailsComponent implements OnInit, OnDestroy {
  cocktails: Observable<null | Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;
  userSub!: Subscription;
  userId!: string;
  api = environment.apiUrl;

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
    this.cocktails = this.store.select(state => state.cocktails.cocktails);
    this.loading = this.store.select(state => state.cocktails.fetchLoading);
    this.error = this.store.select(state => state.cocktails.fetchError)
  }

  ngOnInit(): void {
    this.userSub = this.user.subscribe( user => {
      this.userId = <string>user?._id;
    });
    this.store.dispatch(fetchUserCocktailRequest({id: this.userId}));
  }

  onPublish(id: string) {
    this.store.dispatch(publishRequest({id}))
  }

  removeCocktail(id: string) {
    this.store.dispatch(removeCocktailRequest({id}))
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

}
