import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail } from '../../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute } from '@angular/router';
import { fetchOneCocktailRequest } from '../../store/cocktails.actions';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cocktail',
  templateUrl: './cocktail.component.html',
  styleUrls: ['./cocktail.component.sass']
})
export class CocktailComponent implements OnInit{
  cocktail: Observable<null | Cocktail>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  api = environment.apiUrl;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.cocktail = this.store.select(state => state.cocktails.cocktail);
    this.loading = this.store.select(state => state.cocktails.fetchLoading);
    this.error = this.store.select(state => state.cocktails.fetchError)
  }

  ngOnInit(): void {
    this.route.params.subscribe( params => {
      this.store.dispatch(fetchOneCocktailRequest({id: params['id']}));
    });
  }
}
