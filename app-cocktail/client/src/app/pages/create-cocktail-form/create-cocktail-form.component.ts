import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CocktailData } from '../../models/cocktail.model';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createCocktailsRequest } from '../../store/cocktails.actions';

@Component({
  selector: 'app-create-cocktail-form',
  templateUrl: './create-cocktail-form.component.html',
  styleUrls: ['./create-cocktail-form.component.sass']
})
export class CreateCocktailFormComponent implements OnInit, OnDestroy {
  user: Observable<null | User>;
  userSub!: Subscription;
  userId!: string;
  userRole!: string;
  reactiveForm!: FormGroup;
  isAddIng = true;

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSub = this.user.subscribe( user => {
      this.userId = <string>user?._id;
      this.userRole = <string>user?.role;
    });
    this.reactiveForm = new FormGroup({
      name: new FormControl('', Validators.required),
      image: new FormControl(''),
      recipe: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
    })
  }

  onSubmit() {
    const cocktail = <CocktailData> this.reactiveForm.value
    cocktail.user = this.userId;
    cocktail.isPublished = this.userRole !== 'user';
    this.store.dispatch(createCocktailsRequest({cocktailsData: cocktail}))
  }

  addIngredients() {
    this.isAddIng = false;
    const ingredients = <FormArray>this.reactiveForm.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ing: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
    })
    ingredients.push(ingredientsGroup);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.reactiveForm.get(fieldName);
    return Boolean(
      field && field.touched && field.errors?.[errorType]
    );
  }

  fieldIngredientsError(fieldName: string, index: number, errorType: string) {
    const ingredients = <FormArray>this.reactiveForm.get('ingredients');
    const field = ingredients.controls[index]?.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  getIngredients() {
    return (<FormArray>this.reactiveForm.get('ingredients')).controls;
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
