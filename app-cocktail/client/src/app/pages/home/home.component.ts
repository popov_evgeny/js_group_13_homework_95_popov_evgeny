import { Component, OnInit } from '@angular/core';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Cocktail } from '../../models/cocktail.model';
import { fetchCocktailsRequest, publishRequest, removeCocktailRequest } from '../../store/cocktails.actions';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  cocktails: Observable<null | Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  api = environment.apiUrl;

  constructor(private store: Store<AppState>) {
    this.cocktails = this.store.select(state => state.cocktails.cocktails);
    this.loading = this.store.select(state => state.cocktails.fetchLoading);
    this.error = this.store.select(state => state.cocktails.fetchError)
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());
  }

  onPublish(id: string) {
    this.store.dispatch(publishRequest({id}))
  }

  removeCocktail(id: string) {
    this.store.dispatch(removeCocktailRequest({id}))
  }
}
