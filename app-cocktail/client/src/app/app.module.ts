import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { localStorageSync } from 'ngrx-store-localstorage';
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialAuthServiceConfig,
  SocialLoginModule
} from 'angularx-social-login';
import { environment } from '../environments/environment';
import { userReducer } from './store/users.reducer';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { MatMenuModule } from '@angular/material/menu';
import { UserTypeDirective } from './directives/user-type.directive';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/users.effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginFormComponent } from './pages/login/login-form.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { AuthInterceptor } from './auth.interceptor';
import { HomeComponent } from './pages/home/home.component';
import { cocktailsReducer } from './store/cocktails.reducer';
import { CocktailsEffects } from './store/cocktails.effects';
import { CocktailComponent } from './pages/cocktail/cocktail.component';
import { CreateCocktailFormComponent } from './pages/create-cocktail-form/create-cocktail-form.component';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { UserCocktailsComponent } from './pages/user-cocktails/user-cocktails.component';
import { HasRolesDirective } from './directives/has-roles.directive';
import { OpenedRolesDirective } from './directives/opened-roles.directive';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ImagePipe } from './ui/pipes/image.pipe';
import { NotFoundComponent } from './not-found.component';

export const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{'users': ['user']}],
    rehydrate: true
  })(reducer);
}

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbAppId, {
        scope: 'email,public_profile'
      })
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(environment.googleID)
    }
  ]
}

const metaReducers: Array<MetaReducer> = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    UserTypeDirective,
    LoginFormComponent,
    HomeComponent,
    CocktailComponent,
    CreateCocktailFormComponent,
    FileInputComponent,
    UserCocktailsComponent,
    HasRolesDirective,
    OpenedRolesDirective,
    ImagePipe,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatSnackBarModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      users: userReducer,
      cocktails: cocktailsReducer,
    }, {metaReducers}),
    EffectsModule.forRoot([UsersEffects, CocktailsEffects]),
    MatMenuModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    SocialLoginModule,
    MatProgressSpinnerModule,

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
