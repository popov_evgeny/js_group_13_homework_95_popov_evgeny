import { createAction, props } from '@ngrx/store';
import { Cocktail, CocktailData } from '../models/cocktail.model';

export const fetchCocktailsRequest = createAction('[Cocktails] Fetch Request');
export const fetchCocktailsSuccess = createAction('[Cocktails] Fetch Success', props<{cocktails: Cocktail[]}>());
export const fetchCocktailsFailure = createAction('[Cocktails] Fetch Failure', props<{error: string}>());

export const fetchOneCocktailRequest = createAction('[Cocktails] Fetch One Request', props<{id: string}>());
export const fetchOneCocktailSuccess = createAction('[Cocktails] Fetch One Success', props<{cocktail: Cocktail}>());
export const fetchOneCocktailFailure = createAction('[Cocktails] Fetch One Failure', props<{error: string}>());

export const fetchUserCocktailRequest = createAction('[Cocktails] Fetch User Request', props<{id: string}>());
export const fetchUserCocktailSuccess = createAction('[Cocktails] Fetch User Success', props<{cocktails: Cocktail[]}>());
export const fetchUserCocktailFailure = createAction('[Cocktails] Fetch User Failure', props<{error: string}>());

export const publishRequest = createAction('[Cocktails] Publish Request', props<{id: string}>());
export const publishSuccess = createAction('[Cocktails] Publish Success', props<{cocktails: Cocktail[]}>());

export const createCocktailsRequest = createAction('[Cocktails] Create Request', props<{cocktailsData: CocktailData}>());
export const createCocktailsSuccess = createAction('[Cocktails] Create Success');
export const createCocktailsFailure = createAction('[Cocktails] Create Failure', props<{error: string}>());

export const removeCocktailRequest = createAction('[Cocktails] Remove Request', props<{id: string}>());
export const removeCocktailSuccess = createAction('[Cocktails] Remove Success', props<{cocktails: Cocktail[]}>());
