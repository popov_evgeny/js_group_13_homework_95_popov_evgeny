import { createReducer, on } from '@ngrx/store';
import {
  createCocktailsFailure,
  createCocktailsRequest,
  createCocktailsSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchOneCocktailFailure,
  fetchOneCocktailRequest,
  fetchOneCocktailSuccess,
  fetchUserCocktailFailure,
  fetchUserCocktailRequest,
  fetchUserCocktailSuccess,
  publishRequest,
  publishSuccess,
  removeCocktailRequest,
  removeCocktailSuccess
} from './cocktails.actions';
import { CocktailsState } from './types';

const initialState: CocktailsState = {
  cocktail: null,
  cocktails: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailsSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchCocktailsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchOneCocktailRequest, state => ({...state, fetchLoading: true})),
  on(fetchOneCocktailSuccess, (state, {cocktail}) => ({...state, fetchLoading: false, cocktail})),
  on(fetchOneCocktailFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUserCocktailRequest, state => ({...state, fetchLoading: true})),
  on(fetchUserCocktailSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchUserCocktailFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(publishRequest, state => ({...state, fetchLoading: true})),
  on(publishSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),

  on(createCocktailsRequest, state => ({...state, createLoading: true})),
  on(createCocktailsSuccess, state => ({...state, createLoading: false})),
  on(createCocktailsFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),

  on(removeCocktailRequest, state => ({...state, fetchLoading: true})),
  on(removeCocktailSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
);
