import { LoginError, User } from '../models/user.model';
import { Cocktail } from '../models/cocktail.model';

export type UserState = {
  user: null | User,
  loginLoading: boolean,
  loadingFb: boolean,
  loginError: null | LoginError
};

export type CocktailsState = {
  cocktail: null | Cocktail,
  cocktails: null | Cocktail[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type AppState = {
  users: UserState,
  cocktails: CocktailsState
}
