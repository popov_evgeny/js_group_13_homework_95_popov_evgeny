import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createCocktailsFailure,
  createCocktailsRequest,
  createCocktailsSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchOneCocktailFailure,
  fetchOneCocktailRequest,
  fetchOneCocktailSuccess,
  fetchUserCocktailFailure,
  fetchUserCocktailRequest,
  fetchUserCocktailSuccess,
  publishRequest,
  publishSuccess,
  removeCocktailRequest,
  removeCocktailSuccess
} from './cocktails.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { CocktailsService } from '../services/cocktails.service';
import { HelpersService } from '../services/helpers.service';

@Injectable()
export class CocktailsEffects {
  constructor(
    private actions: Actions,
    private cocktailsService: CocktailsService,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  fetchCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailsService.getCocktails('').pipe(
      map(cocktails => fetchCocktailsSuccess({cocktails})),
      catchError(() => of(fetchCocktailsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchCocktail = createEffect(() => this.actions.pipe(
    ofType(fetchOneCocktailRequest),
    mergeMap(({id}) => this.cocktailsService.getCocktail(id).pipe(
      map(cocktail => fetchOneCocktailSuccess({cocktail})),
      catchError(() => of(fetchOneCocktailFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchUserCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchUserCocktailRequest),
    mergeMap(({id}) => this.cocktailsService.getCocktails(id).pipe(
      map(cocktails => fetchUserCocktailSuccess({cocktails})),
      catchError(() => of(fetchUserCocktailFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  publishCocktails = createEffect(() => this.actions.pipe(
    ofType(publishRequest),
    mergeMap(({id}) => this.cocktailsService.publishCocktails(id).pipe(
      map(cocktails => publishSuccess({cocktails})),
      tap(() => {
        this.helpers.openSnackbar('Your cocktail is being reviewed by a moderator');
      })
    ))
  ));

  createCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailsRequest),
    mergeMap(({cocktailsData}) => this.cocktailsService.createCocktail(cocktailsData).pipe(
      map(() => createCocktailsSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createCocktailsFailure({error: 'Wrong data'})))
    ))
  ));

  removeCocktails = createEffect(() => this.actions.pipe(
    ofType(removeCocktailRequest),
    mergeMap(({id}) => this.cocktailsService.removeCocktails(id).pipe(
      map(cocktails => removeCocktailSuccess({cocktails})),
      tap(() => {
        this.helpers.openSnackbar('Cocktail is delete!');
      })
    ))
  ));
}
