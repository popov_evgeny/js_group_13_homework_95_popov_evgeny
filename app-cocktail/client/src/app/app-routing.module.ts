import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginFormComponent } from './pages/login/login-form.component';
import { HomeComponent } from './pages/home/home.component';
import { CocktailComponent } from './pages/cocktail/cocktail.component';
import { CreateCocktailFormComponent } from './pages/create-cocktail-form/create-cocktail-form.component';
import { UserCocktailsComponent } from './pages/user-cocktails/user-cocktails.component';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginFormComponent},
  { path: ':id/:name/cocktail', component: CocktailComponent},
  { path: 'create-new-cocktail', component: CreateCocktailFormComponent},
  { path: 'my_cocktails', component: UserCocktailsComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
