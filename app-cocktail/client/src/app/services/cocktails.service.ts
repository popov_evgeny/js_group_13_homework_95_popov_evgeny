import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiCocktailData, Cocktail, CocktailData } from '../models/cocktail.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CocktailsService {
  constructor(private http: HttpClient) { }

  getCocktails(id: string) {
    const query = '?user=' + id;
    return this.http.get<ApiCocktailData[]>(environment.apiUrl + '/cocktails' + query).pipe(
      map(response => {
        return response.map(cocktailData => {
          return new Cocktail(
            cocktailData._id,
            cocktailData.name,
            cocktailData.user,
            cocktailData.image,
            cocktailData.recipe,
            cocktailData.isPublished,
            cocktailData.ingredients,
          );
        });
      })
    );
  }

  getCocktail(id: string) {
    return this.http.get<Cocktail>(environment.apiUrl + '/cocktails/' + id);
  }

  createCocktail(cocktailData: CocktailData) {
    const formData = new FormData();

    Object.keys(cocktailData).forEach(key => {
      if (cocktailData[key] !== null) {
        if (key !== 'ingredients') {
          formData.append(key, cocktailData[key]);
        } else {
          formData.append(key, JSON.stringify(cocktailData[key]));
        }
      }
    });

    return this.http.post(environment.apiUrl + '/cocktails', formData);
  }

  publishCocktails(id: string) {
    return this.http.put<ApiCocktailData[]>(environment.apiUrl + '/cocktails/publish', {id: id}).pipe(
      map(response => {
        return response.map(cocktailData => {
          return new Cocktail(
            cocktailData._id,
            cocktailData.name,
            cocktailData.user,
            cocktailData.image,
            cocktailData.recipe,
            cocktailData.isPublished,
            cocktailData.ingredients,
          );
        });
      })
    );
  }

  removeCocktails(id: string) {
    return this.http.delete<ApiCocktailData[]>(environment.apiUrl + '/cocktails/remove/' + id).pipe(
      map(response => {
        return response.map(cocktailData => {
          return new Cocktail(
            cocktailData._id,
            cocktailData.name,
            cocktailData.user,
            cocktailData.image,
            cocktailData.recipe,
            cocktailData.isPublished,
            cocktailData.ingredients,
          );
        });
      })
    );
  }
}
