const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Cocktails = require("./models/Cocktail");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const Jack = await User.create({
    email: 'test@test.com',
    password: '123',
    name: 'Jack Doe',
    token: nanoid(),
    role: 'admin',
    avatar: 'fun.jpg'
  });

  const [Margarita, Gin_pom, Paloma_cocktail] = await Cocktails.create({
    name: 'Margarita',
    user: Jack,
    image: 'margo.webp',
    recipe: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur hic magnam minima nisi possimus praesentium qui ratione repellendus similique voluptates!',
    isPublished: true,
    ingredients: [{
      ing: 'ice',
      amount: '5g'
    }]
  },{
    name: 'Gin_pom',
    user: Jack,
    image: 'gin.jpg',
    recipe: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur hic magnam minima nisi possimus praesentium qui ratione repellendus similique voluptates!',
    isPublished: false,
    ingredients: [{
      ing: 'gin',
      amount: '50ml'
    }]
  },{
    name: 'Paloma_cocktail',
    user: Jack,
    image: 'paloma.jpg',
    recipe: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur hic magnam minima nisi possimus praesentium qui ratione repellendus similique voluptates!',
    isPublished: false,
    ingredients: [{
      ing: 'tequila',
      amount: '50ml'
    }]
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));