const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IngredientsSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  image: null | String,
  recipe: {
    type: String,
    required: true
  },
  isPublished: {
    type: Boolean,
    required: true,
    default: false,
  },
  ingredients: {
    type: [{}],
    required: true
  }
});

const Ingredients = mongoose.model('Ingredients', IngredientsSchema);

module.exports = Ingredients;