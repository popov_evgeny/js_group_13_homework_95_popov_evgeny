const path = require('path');
const fs = require("fs").promises;
const express = require('express');
const multer = require('multer');
const { nanoid } = require('nanoid');
const config = require('../config');
const Cocktails = require("../models/Cocktail");
const mongoose = require("mongoose");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadsPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    let query = {}
    if (req.query.user) {
      query.user = {_id: req.query.user}
    }

    const cocktails = await Cocktails.find(query).populate("user", "name");

    return res.send(cocktails);
  } catch (e) {
    next(e);
  }
});

router.put('/publish', async (req, res, next) => {
  try {
    await Cocktails.updateOne({_id: req.body.id}, {isPublished: true});
    const cocktails = await Cocktails.find().populate("user", "name");

    return res.send(cocktails);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const cocktail = await Cocktails.findById(req.params.id);

    if (!cocktail) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(cocktail);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    const cocktailData = {
      name: req.body.name,
      user: req.body.user,
      image: null,
      recipe: req.body.recipe,
      isPublished: false,
      ingredients: JSON.parse(req.body.ingredients)
    };

    if (req.file) {
      cocktailData.image = req.file.filename;
    }

    if (req.body.isPublished) {
      cocktailData.isPublished = req.body.isPublished;
    }

    const cocktail = new Cocktails(cocktailData);

    await cocktail.save();

    return res.send({message: 'Created new product', id: cocktail._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      if (req.file) {
        await fs.unlink(req.file.path);
      }
      return res.status(400).send(e);
    }
    next(e);
  }
});

router.delete('/remove/:id', async (req, res, next) => {
  try {
    await Cocktails.deleteOne({_id: req.params.id});
    const cocktails = await Cocktails.find().populate("user", "name");

    return res.send(cocktails);
  } catch (e) {
    next(e);
  }
});

module.exports = router;